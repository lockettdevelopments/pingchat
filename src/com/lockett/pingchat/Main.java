package com.lockett.pingchat;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
//
	@Override
	public void onEnable() {
		getLogger().info("PingChat developed by Lockett.");
		Bukkit.getPluginManager().registerEvents(new PingListner(), this);
		getCommand("ping").setExecutor(new PingCommand());
	}
	
	
}
