
package com.lockett.pingchat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PingCommand implements CommandExecutor {
//
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (sender instanceof Player) {
			String noperm = "You don't have permission for this.";
			Player player = (Player) sender;
			if (player.hasPermission("ping.use") || player.hasPermission("ping.other")) {
				if (args.length == 0) {
					int ping = ((CraftPlayer) player).getHandle().ping;
					player.sendMessage(ChatColor.AQUA + "Your ping: " + ping + "ms");
				}
				if (args.length == 1) {
					if (player.hasPermission("ping.other")) {
						if (Bukkit.getPlayerExact(args[0]) != null) {
							Player target = Bukkit.getPlayerExact(args[0]);
							int ping = ((CraftPlayer) target).getHandle().ping;
							player.sendMessage(ChatColor.AQUA + target.getName() + "'s ping: " + ping + "ms" );
						} else {
							player.sendMessage(ChatColor.RED + "Player not found.");
						}
					} else {
						player.sendMessage(ChatColor.RED + noperm);
					}
				}
			} else {
				player.sendMessage(ChatColor.RED + noperm);
			}
		
		
		} else {
			if (args.length == 1) {
				if (Bukkit.getPlayerExact(args[0]) != null) {
					Player target = Bukkit.getPlayerExact(args[0]);
					int ping = ((CraftPlayer) target).getHandle().ping;
					System.out.println(target.getName() + "'s ping: " + ping + "ms");
				} else {
					System.out.println("Player not found.");
				}
			} else {
				System.out.println("Use ping <player>");
			}
		}
		return false;
		
	}
	}

