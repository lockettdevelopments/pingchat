package com.lockett.pingchat;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PingListner implements Listener {
//
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
			
			String msg = e.getMessage();
		if (player.hasPermission("ping.use")) {	
		if (msg.contains("#ping")) {
			int ping = ((CraftPlayer) player).getHandle().ping;
			
			
			
			e.setCancelled(true);
			String message = msg.replaceAll("#ping", "" + ChatColor.GOLD + "[" + ChatColor.DARK_PURPLE + ping + "ms" + ChatColor.GOLD + "]" + ChatColor.RESET);
			player.chat(message);
			
		}
		} else {
			player.sendMessage(ChatColor.RED + "Couldn't replace #ping because you don't have permission to do this.");
		}
	}
	
}
