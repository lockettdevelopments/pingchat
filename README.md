### How to install? ###
You can download this through the downloads section here, or find this on Spigot.
1) Place the .jar in your servers plugins folder
2) Reload/Restart your server.

### Issues and Support ###
You can get support, and report issues through the issue tracker on this repository.